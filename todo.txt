* Move the restaurant list out of the script.

* Add a classification of slow, semi-slow and fast for restaurants.  Andrew's
definition of slow, semi-slow and fast is below.  My thought is that we should
time how long it takes at the restaurant and average it over several trips.  I
asked if this is something we'd really use.  No response, so this probably
won't happen.

Fast (not fastfood, no one ever goes there) -
Places like boston market/rubios where you order your food then pick it
up yourself. These tend to be the quickest places.

Semi-slow -
Places that we go to when we order first (individually), sit down then
get it delivered to us. (panera, pei wei, pat and Oscars)

Slow - 
Places that we sit down, have a waiter/tress, get a menu/drinks, order
(as a group), and have a food delivered to us. (Thai, Mama Cellas,
Islands)

* Attempt to code to GNU Coding Standards
  http://www.gnu.org/prep/standards/
