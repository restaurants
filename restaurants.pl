#!/usr/bin/perl

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

use warnings;
use strict;

use Getopt::Long;

my @restaurant_list = (
   '4S Ranch (Outside Patio)',
   'Akai Hana',
   'Athens',
   'Baja Fresh',
   'Bamboo Fresh',
   'Boston Market',
   'Brett\'s BBQ',
   'California Pizza Kitchen',
   'Cheeburger Cheeburger',
   'Chili\'s',
   'China Fun',
   'Coco\'s',
   'Daphne\'s Greek Cafe',
   'El Torito\'s',
   'Elephant Bar',
   'Fire House Pizza',
   'Grazianos',
   'Island\'s',
   'Joey\'s Smokin\' BBQ',
   'Kelly\'s Public House',
   'King Taco Loco Donalds In-N-Out a Box Jr\'s',
   'L&L Hawaiian Barbecue',
   'Little Tokyo',
   'Mama Cella\'s',
   'Marie Callender\'s',
   'Olive Garden',
   'Panda Buffet',
   'Panera',
   'Passage To India',
   'Pat and Oscars',
   'Pearl',
   'Pegasus',
   'Pei Wei',
   'Pho',
   'Pick Up Stix',
   'Quick Wok',
   'RB Sushi',
   'Rubio\'s',
   'Souplantation',
   'Spices Thai',
   'Stir Fresh',
   'Subway',
   'ThaiGo',
   'ToGo\'s',
   'Wahoo\'s',
   'Wings N Things');

sub main {
   my $pick = 1;
   my $quiet = 0;
   my @random_restaurant_list;

   GetOptions(
      'pick=i' => \$pick,
      'quiet!' => \$quiet,
      'silent!' => \$quiet);

   if(1 > $pick) {
      print "Usage: . . . \n";
      return -1;
   }

   if(1 != $quiet) {
      print "Karen's Restaurant Selector\n\n";
   }

   # Create list
   my $done = 0;
   my $num_of_loops = 0;
   do {
      $num_of_loops++;
      my $restaurant_name = $restaurant_list[rand @restaurant_list];
      my $on_list = 0;
      foreach (@random_restaurant_list) {
         if ($_ eq $restaurant_name) {
            $on_list = 1;
         }
      }
      if (0 == $on_list) {
         push @random_restaurant_list, $restaurant_name;
      }
      if($pick == scalar @random_restaurant_list) {
         $done = 1;
      }

      # If we have tried to pick restaurant twice as many times as asked and
      # still don't have enough, just exit.
      if($num_of_loops >= $pick * 2) {
         $done = 1;
      }
   } until(1 == $done);

   # Print list
   @random_restaurant_list = sort @random_restaurant_list;
   my $num_restaurants = scalar @random_restaurant_list;
   for(my $i = 0; $i < $num_restaurants; $i++) {
      print $random_restaurant_list[$i];
      if($i + 1 != $num_restaurants) {
         print ", ";
      }
   }
   print "\n";

   return 0;
}

exit(main());
